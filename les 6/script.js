function Human(name,surname,age){
    this.name = name;
    this.surname = surname;
    this.age = age;
}

Human.prototype.eldest = function (humans){
    var maxAge = 0
    humans.forEach(function (human){
        if  (human.age > maxAge)
            maxAge = human.age
    })
alert(maxAge)

}
let human = new Human()
var humans = []


humans.push(new Human("Max", "unknown", 18));
humans.push(new Human("Ivan", "unknown",39));
humans.push(new Human("Valera", "unknown",23));
humans.push(new Human("Gleb", "unknown", 49));

human.eldest(humans)
